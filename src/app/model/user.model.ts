export class UserModel {
  public name: string;
  public phone: string;
  public email: string;
  public company: string;
  public dateEntry: string;
  public orgNum: string;
  public address1: string;
  public city: string;
  public zip: string;
  public geo: string;
  public pan: string;
  public pin: string;
  public id: any;
  public status: string;
  public fee: string;
  public guid: string;
  public dateRecent: string;
  public dateExit: string;
  public dateFirst: string;
  public url: string;
  constructor(
    name: string,
    phone: string,
    email: string,
    company: string,
    dateEntry: string,
    orgNum: string,
    address1: string,
    city: string,
    zip: string,
    geo: string,
    pan: string,
    pin: string,
    id: any,
    status: string,
    fee: string,
    guid: string,
    dateExit: string,
    dateFirst: string,
    dateRecent: string,
    url: string
  ) {
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.company = company;
    this.dateEntry = dateEntry;
    this.orgNum = orgNum;
    this.address1 = address1;
    this.city = city;
    this.zip = zip;
    this.geo = geo;
    this.pan = pan;
    this.pin = pin;
    this.id = id;
    this.status = status;
    this.fee = fee;
    this.guid = guid;
    this.dateExit = dateExit;
    this.dateFirst = dateFirst;
    this.dateRecent = dateRecent;
    this.url = url;
  }
}
