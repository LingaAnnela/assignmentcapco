import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UsersComponent } from './users.component';
import { UserService } from 'src/app/services/user.service';
import { of } from 'rxjs';
import { UserModel } from 'src/app/model/user.model';
const mockData: UserModel[] = [{
  name: 'Vivien F. Lawson',
  phone: '541-0880',
  email: 'sed.consequat@scelerisquesed.net',
  company: 'Sagittis Nullam Vitae Associates',
  dateEntry: '2018-02-05 05:49:12',
  orgNum: '902166 3704',
  address1: 'P.O. Box 843, 6886 Tellus Road',
  city: 'West Valley City',
  zip: '9136',
  geo: '-32.64919, -6.93157',
  pan: '492906 6342799278',
  pin: '6708',
  id: 99,
  status: 'unread',
  fee: '$67.51',
  guid: '390340AD-677F-42E2-569E-42E7D212DCAC',
  dateExit: '2017-04-14 10:08:38',
  dateFirst: '2019-03-06 05:29:21',
  dateRecent: '2018-04-06 22:30:38',
  url: 'https://capco.com/'
}, {
  name: 'Linga X Annela',
  phone: '541-0880',
  email: 'sed.consequat@scelerisquesed.net',
  company: 'Sagittis Nullam Vitae Associates',
  dateEntry: '2018-02-05 05:49:12',
  orgNum: '902166 3704',
  address1: 'P.O. Box 843, 6886 Tellus Road',
  city: 'West Valley City',
  zip: '9136',
  geo: '-32.64919, -6.93157',
  pan: '492906 6342799278',
  pin: '6708',
  id: 99,
  status: 'unread',
  fee: '$67.51',
  guid: '390340AD-677F-42E2-569E-42E7D212DCAC',
  dateExit: '2017-04-14 10:08:38',
  dateFirst: '2019-03-06 05:29:21',
  dateRecent: '2018-04-06 22:30:38',
  url: 'https://capco.com/'
}];

const mockData2 = [];

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let userService: UserService;

  let httpClientSpy: { get: jasmine.Spy };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [UsersComponent],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    fixture.detectChanges();
    spyOn(userService, 'getUsers').and.returnValue(of(mockData));
    spyOn(component, 'changeNumberOfPages').and.callThrough();

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);

  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should render title in a h2 tag on users component', () => {
    // const fixture = TestBed.createComponent(UsersComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Users list with pagination!');
  });

  it('should create the table on user component', () => {
    // const fixture = TestBed.createComponent(UsersComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('table').id).toContain('user-table');
  });

  it('changeNumberOfPages', () => {
    component.usersArray = mockData;
    component.changeNumberOfPages(10);

    expect(component.numberOfRecords).toBe(10);
  });

  it('click next/previous button to fetch next five pages ', () => {
    component.usersArray = mockData;
    component.getNextPrevPages(true);

    expect(component.startIndex).toBe(5);
    expect(component.lastIndex).toBe(9);

    component.getNextPrevPages(false);

    expect(component.startIndex).toBe(0);
    expect(component.lastIndex).toBe(4);

  });

  it('check number of records per page', () => {
    component.usersArray = mockData;
    component.getUsersPerPage(1, 10);

    expect(component.numberOfRecords).toBe(10);

    component.getUsersPerPage(1, 20);

    expect(component.numberOfRecords).toBe(20);

    component.getUsersPerPage(1, 30);

    expect(component.numberOfRecords).toBe(30);

    component.getUsersPerPage(1, 40);

    expect(component.numberOfRecords).toBe(40);

  });

  it('Check for no records found', () => {
    component.usersArray = mockData2;
    component.getUsersPerPage(1, 10);

    expect(component.recordsNotFound).toBeTruthy();

    component.getUsersPerPage(1, 20);

    expect(component.recordsNotFound).toBeTruthy();

    component.getUsersPerPage(1, 30);

    expect(component.recordsNotFound).toBeTruthy();

    component.getUsersPerPage(1, 40);

    expect(component.recordsNotFound).toBeTruthy();

  });

  it('Check for records found!', () => {
    component.usersArray = mockData;
    component.getUsersPerPage(1, 10);

    expect(component.recordsNotFound).toBeFalsy();

    component.getUsersPerPage(1, 20);

    expect(component.recordsNotFound).toBeFalsy();

    component.getUsersPerPage(1, 30);

    expect(component.recordsNotFound).toBeFalsy();

    component.getUsersPerPage(1, 40);

    expect(component.recordsNotFound).toBe(false);

  });

});
