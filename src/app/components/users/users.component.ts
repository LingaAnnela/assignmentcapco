import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserModel } from '../../model/user.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: UserModel[];
  private users$: Subscription;
  usersArray: UserModel[];

  noOfRecordsPerPage: number[]; // dropdown
  numberOfRecords: number; // default value
  totalPages: number;
  paginationList: any[];
  disablePrev: boolean;
  disableNext: boolean;
  thisPage: number;
  startIndex: number;
  lastIndex: number;
  recordsNotFound: boolean;

  constructor(private fetchDataSvc: UserService) { }

  ngOnInit() {
    this.noOfRecordsPerPage = [10, 20, 30, 50]; // dropdown
    this.numberOfRecords = 10; // default value
    this.totalPages = 0;
    this.disablePrev = true;
    this.disableNext = true;
    this.recordsNotFound = false;
    this.thisPage = 0;
    this.startIndex = 0;
    this.lastIndex = 0;

    this.getUsers();
    // To display the pagination elements
    this.paginationList = Array(5)
      .fill(0)
      .map((x, i) => i);
  }

  changeNumberOfPages(value) {
    this.numberOfRecords = value;
    this.disablePrev = true;
    this.thisPage = 0;
    // If the number of records per page are changed then it will display from the first record.
    // To display the pagination elements
    this.paginationList = Array(5)
      .fill(0)
      .map((x, i) => i);
    this.getUsersPerPage(1, value);
  }

  getUsersPerPage(startVal: number, records: number) {
    records = records ? records : this.numberOfRecords;
    this.numberOfRecords = records;
    // Usually we have to make another API call to fetch the records, but here this logic suffice.
    this.users = this.usersArray.slice(
      (startVal - 1) * records,
      startVal * records
    );

    this.recordsNotFound = this.users.length > 0 ? false : true;

    this.totalPages = Math.ceil(this.usersArray.length / records);

    this.prevNextButtonStatus();

    // Add active class to the current button (highlight it)
    const header = document.getElementById('footer-nav');
    if (header) {
      const btns = header.getElementsByClassName('container-footer--button');
      // btns[0].classList.add('btn-active');
      /*
      for (let i = 0; i < btns.length; i++) {
        if (parseInt(btns[i].innerHTML, 10) === startVal) {
          btns[i].classList.add('btn-active');
        } else {
          btns[i].classList.remove('btn-active');
        }
      }
      */

      Array.prototype.forEach.call(btns, (btn) => {
        if (parseInt(btn.innerHTML, 10) === startVal) {
          btn.classList.add('btn-active');
        } else {
          btn.classList.remove('btn-active');
        }
    });
    }
  }

  getNextPrevPages(isNextPage: boolean) {
    // this.thisPage = isNextPage ? this.thisPage++ : this.thisPage--;
    if (isNextPage) {
      this.thisPage = this.thisPage + 1;
    } else {
      this.thisPage = this.thisPage - 1;
    }
    this.paginationList = Array(5)
      .fill(0)
      .map((x, i) => {
        return i + 5 * this.thisPage;
      });

    this.startIndex = this.paginationList[0];
    this.lastIndex = this.paginationList[this.paginationList.length - 1];
    console.log(this.startIndex + ' : ' + this.lastIndex);

    console.log(this.paginationList);
    this.getUsersPerPage(this.paginationList[0] + 1, this.numberOfRecords);
    this.prevNextButtonStatus();
  }

  prevNextButtonStatus() {
    this.disablePrev = this.thisPage === 0 ? true : false;
    this.disableNext =
      this.totalPages <= this.paginationList[this.paginationList.length - 1] + 1
        ? true
        : false;
  }

  onSubmit(id: string, status: string) {
    console.log(id + ' : ' + status);

    this.fetchDataSvc.postUser(id, status).subscribe(
      res => {
        console.log('The user has been posted!.');
        alert('Mock user submitted for id : ' + id);
      },
      error => console.log('Error :: ' + error)
    );
  }

  getUsers(): void {
    this.fetchDataSvc.getUsers().subscribe(
      data => {
        this.usersArray = data;
        this.getUsersPerPage(1, 10);
        console.log(this.usersArray);
      },
      error => {
        console.log('Error :: ' + error);
        this.recordsNotFound = true;
      }
    );
  }
}
