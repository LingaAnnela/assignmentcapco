import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersListComponent } from './users-list.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from 'src/app/services/user.service';
import { of } from 'rxjs';
import { UserModel } from 'src/app/model/user.model';

const mockData: UserModel[] = [{
  name: 'Vivien F. Lawson',
  phone: '541-0880',
  email: 'sed.consequat@scelerisquesed.net',
  company: 'Sagittis Nullam Vitae Associates',
  dateEntry: '2018-02-05 05:49:12',
  orgNum: '902166 3704',
  address1: 'P.O. Box 843, 6886 Tellus Road',
  city: 'West Valley City',
  zip: '9136',
  geo: '-32.64919, -6.93157',
  pan: '492906 6342799278',
  pin: '6708',
  id: 99,
  status: 'unread',
  fee: '$67.51',
  guid: '390340AD-677F-42E2-569E-42E7D212DCAC',
  dateExit: '2017-04-14 10:08:38',
  dateFirst: '2019-03-06 05:29:21',
  dateRecent: '2018-04-06 22:30:38',
  url: 'https://capco.com/'
}, {
  name: 'Linga X Annela',
  phone: '541-0880',
  email: 'sed.consequat@scelerisquesed.net',
  company: 'Sagittis Nullam Vitae Associates',
  dateEntry: '2018-02-05 05:49:12',
  orgNum: '902166 3704',
  address1: 'P.O. Box 843, 6886 Tellus Road',
  city: 'West Valley City',
  zip: '9136',
  geo: '-32.64919, -6.93157',
  pan: '492906 6342799278',
  pin: '6708',
  id: 99,
  status: 'unread',
  fee: '$67.51',
  guid: '390340AD-677F-42E2-569E-42E7D212DCAC',
  dateExit: '2017-04-14 10:08:38',
  dateFirst: '2019-03-06 05:29:21',
  dateRecent: '2018-04-06 22:30:38',
  url: 'https://capco.com/'
}];

describe('UsersListComponent', () => {
  let component: UsersListComponent;
  let fixture: ComponentFixture<UsersListComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [UsersListComponent],
      providers: [UserService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersListComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    fixture.detectChanges();
    spyOn(userService, 'getUsers').and.returnValue(of(mockData));
    // spyOn(component, 'getUsers').and.returnValue(of(mockData));
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should render title in a h2 tag on users list component', () => {
    // const fixture = TestBed.createComponent(UsersListComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Users list without pagination!');
  });

  it('should create the table on user list component', () => {
    // const fixture = TestBed.createComponent(UsersListComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('table').id).toBe('userlist-table');
  });

  it('should call getUsers method onInit', () => {
    component.ngOnInit();
    expect(userService.getUsers).toHaveBeenCalled();
  });

  it('should call getUsers', () => {
    component.getUsers();
    expect(userService.getUsers).toHaveBeenCalled();
  });

  it('should call getUsers to validate data', () => {
    userService.getUsers().subscribe(data => {
      expect(data).toEqual(mockData);
    });
  });
});
