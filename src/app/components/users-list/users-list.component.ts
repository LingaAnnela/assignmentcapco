import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserModel } from '../../model/user.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  users: any;
  private users$: Subscription;
  usersArray: UserModel[];

  noOfRecordsPerPage: number[]; // dropdown
  numberOfRecords: number; // default value
  totalPages: number;
  paginationList: any[];
  defaultPage: number;
  disablePrev: boolean;
  disableNext: boolean;
  thisPage: number;

  constructor(private fetchDataSvc: UserService) {}

  ngOnInit() {
    // this.defaultPage = 1;
    this.getUsers();
    this.disablePrev = true;
    this.disableNext = false;
    this.thisPage = 0;
    this.totalPages = 0;
    this.numberOfRecords = 10;
    this.noOfRecordsPerPage = [10, 20, 30, 50];
    // To display the pagination elements
    this.paginationList = Array(5)
      .fill(0)
      .map((x, i) => i);
  }

  // getUsersPerPage() {
  //   //Usually we have to make another API call to fetch the records, but here this logic suffice.
  // }

  getUsers(): void {
    this.fetchDataSvc.getUsers().subscribe(
      data => {
        this.usersArray = data;
        this.users = this.usersArray;
        console.log(this.usersArray);
      },
      error => console.log('Error :: ' + error)
    );
  }
}
