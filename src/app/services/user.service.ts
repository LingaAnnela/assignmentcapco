import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {}

  getUsers(): Observable<any> {
    return this.http.get('../../../assets/users.json').pipe(
      map((response: Response) => {
        if ( response ) {
          return response;
        } else {
          return this.logError(response);
        }
      })
    );
  }

  private logError( error: any ) {
    console.error( error.error );
    throw error;
  }

  postUser(id: string, status: string): Observable<any> {
    const body = {id, status};
    return this.http.post<any>(`/api/submit/${id}`, body);
  }
}
