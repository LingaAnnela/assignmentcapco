import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

const mockData = [{
  name: 'Vivien F. Lawson',
  phone: '541-0880',
  email: 'sed.consequat@scelerisquesed.net',
  company: 'Sagittis Nullam Vitae Associates',
  date_entry: '2018-02-05 05:49:12',
  org_num: '902166 3704',
  address_1: 'P.O. Box 843, 6886 Tellus Road',
  city: 'West Valley City',
  zip: '9136',
  geo: '-32.64919, -6.93157',
  pan: '492906 6342799278',
  pin: '6708',
  id: 99,
  status: 'unread',
  fee: '$67.51',
  guid: '390340AD-677F-42E2-569E-42E7D212DCAC',
  date_exit: '2017-04-14 10:08:38',
  date_first: '2019-03-06 05:29:21',
  date_recent: '2018-04-06 22:30:38',
  url: 'https://capco.com/'
}, {
  name: 'Linga X Annela',
  phone: '541-0880',
  email: 'sed.consequat@scelerisquesed.net',
  company: 'Sagittis Nullam Vitae Associates',
  date_entry: '2018-02-05 05:49:12',
  org_num: '902166 3704',
  address_1: 'P.O. Box 843, 6886 Tellus Road',
  city: 'West Valley City',
  zip: '9136',
  geo: '-32.64919, -6.93157',
  pan: '492906 6342799278',
  pin: '6708',
  id: 99,
  status: 'unread',
  fee: '$67.51',
  guid: '390340AD-677F-42E2-569E-42E7D212DCAC',
  date_exit: '2017-04-14 10:08:38',
  date_first: '2019-03-06 05:29:21',
  date_recent: '2018-04-06 22:30:38',
  url: 'https://capco.com/'
}];


describe('UserService', () => {
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });

    userService = TestBed.get(UserService);
    spyOn(userService, 'getUsers').and.returnValue(of(mockData));
  });

  it('should call getUsers', () => {
    userService.getUsers().subscribe(data => {
      expect(data).toEqual(mockData);
    });
  });

  it('should return the list of forms from the server on success', () => {

    userService.getUsers().subscribe((data) => {

      // expect(mockData.length).toBe(3);
      // expect(data[0].title).toBe('Vivien F. Lawson');
      // expect(data[1].name).toBe('Linga X Annela');

    });
  });

  it('should log an error to the console on error', () => {

    spyOn(console, 'error');

    userService.getUsers().subscribe(null, () => {
      expect(console.error).toHaveBeenCalledWith(`I'm afraid I've got some bad news!`);
    });
  });

});
